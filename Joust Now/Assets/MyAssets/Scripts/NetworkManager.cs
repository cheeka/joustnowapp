﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : Photon.PunBehaviour {

    string _gameVersion = "1";
    [SerializeField]
    private GameObject menuPanel;
    [SerializeField]
    private GameObject waitingPanel;

    void Awake()
    {

        PhotonNetwork.autoJoinLobby = false;
        PhotonNetwork.automaticallySyncScene = true;
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        Connect();
    }

    public void Connect()
    {

       
        if(!PhotonNetwork.connected)
        {
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }

    public void FindMatch()
    {
        if (PhotonNetwork.connected)
        {
            menuPanel.SetActive(false);
            waitingPanel.SetActive(true);
            PhotonNetwork.JoinRandomRoom();
            
        }
    }


    #region Photon.PunBehaviour CallBacks


    public override void OnConnectedToMaster()
    {


        Debug.Log("DemoAnimator/Launcher: OnConnectedToMaster() was called by PUN");


    }


    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        Debug.Log("@OnLeftRoom networkManager");
        SceneManager.LoadScene(0);
    }


    public override void OnDisconnectedFromPhoton()
    {


        Debug.LogWarning("DemoAnimator/Launcher: OnDisconnectedFromPhoton() was called by PUN");
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");

        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2 },null);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
      
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        base.OnPhotonPlayerConnected(newPlayer);
        if(PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.LoadLevel("PlayScene");
        }
    }

    #endregion
}
