﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSetup : Photon.PunBehaviour {

    private const string PLAYER1_TAG = "Player1";
    private const string PLAYER2_TAG = "Player2";

    void Start () {
        init();
		
	}

    private void init()
    {
        if(transform.tag.Equals(PLAYER2_TAG))
            GetComponent<SpriteRenderer>().flipX = true;

    }
	
}
