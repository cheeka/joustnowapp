﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerResults : Photon.PunBehaviour
{
    public enum state : byte
    {
        none,
        died,
        blocked,
        avoided
    };

    private state pState;

    public state _pState
    {
        get { return pState; }
        set { pState = value;
       }
    }

    private int animationState;

    public int _animationState
    {
        get { return animationState; }
        set { animationState = value;
            switch(animationState)
            {
                case 0:
                    //idleAnimation
                    break;
                case 1:
                    anim.SetTrigger("isRunning");
                    break;
                case 2:
                    anim.SetTrigger("isAttacking");
                    break;
            }
        }
    }

    [SerializeField]
    private Animator anim;

    private bool isStateDisplayed;

    [SerializeField]
    private GameObject ResultText;

    [SerializeField]
    private bool isReady;

    [SerializeField]
    private GameManager gm;

    public bool _isReady
    {
        get { return isReady; }
        set { isReady = value; }
    }
    [SerializeField]
    private int atkValue;

    public int _atkValue
    {
        get { return atkValue; }
        set { atkValue = value; }
    }
    [SerializeField]
    private int blkValue;

    public int _blkValue
    {
        get { return blkValue; }
        set { blkValue = value; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("@OnTriggerEnter2D PlayerResults");

        if (collision.transform.name.Equals("CenterTrigger") && !isStateDisplayed)
        {
            _animationState = 2;
            if (_pState == state.died)
            {
                GetComponent<SpriteRenderer>().color = Color.red;
            }
            else if (_pState == state.blocked)
            {
                GetComponent<SpriteRenderer>().color = Color.blue;
            }
            else if (_pState == state.avoided)
            {
                GetComponent<SpriteRenderer>().color = Color.green;
            }
            if(photonView.isMine)
            {
                switch (gm._winner)
                {
                    case 0:
                        ResultText.GetComponent<Text>().text = "Draw";
                        break;
                    case 1:
                        ResultText.GetComponent<Text>().text = "Player1 win";
                        break;
                    case 2:
                        ResultText.GetComponent<Text>().text = "Player2 win";
                        break;
                    case 3:
                        ResultText.GetComponent<Text>().text = "Rematch";
                        break;

                }
                ResultText.SetActive(true);
            }
           

            isStateDisplayed = true;
        }


    }

  



    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(isReady);
            stream.SendNext(atkValue);
            stream.SendNext(blkValue);
        }
        else
        {
            isReady = (bool)stream.ReceiveNext();
            atkValue = (int)stream.ReceiveNext();
            blkValue = (int)stream.ReceiveNext();
        }
    }

    private void init()
    {
        isReady = false;
        atkValue = -1;
        blkValue = -1;
        _pState = state.none;
        isStateDisplayed = false;
        ResultText = GameObject.FindGameObjectWithTag("ResultText");
        
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void Start()
    {
        init();
    }

   
}
