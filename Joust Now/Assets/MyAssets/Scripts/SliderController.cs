﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Slider))]
public class SliderController : Photon.PunBehaviour {

    [System.Serializable]
    public struct sliders
    {
        public Sprite sliderImage;
        public float succes;
    }

    [SerializeField]
    private Text result;

    private const string PLAYER1_TAG = "Player1";
    private const string PLAYER2_TAG = "Player2";


    [SerializeField]
    private float redGap = 0.02f;
    [SerializeField]
    private float yellowGap = 0.08f;
    [SerializeField]
    private float greengap = 0f;

    [SerializeField]
    private Slider slider;

    [SerializeField]
    private Image sliderBackground;

    [SerializeField]
    private GameManager gameManager;

    public List<sliders> sliderList;

    private bool isActivated = false;
    private bool isAscending = true;
    public float sliderSpeed = 0.1f;
    private int activeSliderIndex = 0;
    private byte sliderType; // 0 atk 1 Blk

    
    void Start () {
        slider = GetComponent<Slider>();

        sliderBackground.sprite = sliderList[0].sliderImage;
        if(transform.name.Equals("Attack Slider"))
        {
            sliderType = 0;

        }else
        {
            sliderType = 1;
        }
	}
	
	
	void Update () {
		if(isActivated)
        {
            CheckForTap();
            MoveSlider();
            
        }
	}

    private void MoveSlider()
    {
        if(isAscending)
        {
            float v = slider.value + sliderSpeed * Time.deltaTime;
            if(v < 1)
            {
                slider.value = v;
            }
            else
            {
                slider.value = 1f;
                isAscending = false;
            }
        }
        else
        {
            float v = slider.value - sliderSpeed * Time.deltaTime;
            if (v > 0)
            {
                slider.value = v;
            }
            else
            {
                slider.value = 0f;
                isAscending = true;
            }

        }
        
        
    }

    private void CheckForTap()
    {
        
            if (Input.GetMouseButtonDown(0))
            {
            isActivated = false;
            GetResult();
            gameManager.NextPhase();
            }
    }

    public void GetResult()
    {
        float accurate = sliderList[activeSliderIndex].succes - slider.value;
        accurate = Mathf.Abs(accurate);
        Debug.Log("Accurate = " + accurate);
        int v = -1;
        if(accurate <= redGap)
        {
            v = 3;
          //  result.text = "RED";
            Debug.Log("RED!");
        }else if(accurate <= yellowGap)
        {
            v = 2;
          //  result.text = "YELLOW";
            Debug.Log("YELLOW!");
        }else if(accurate <= greengap)
        {
            v = 1;
         //   result.text = "GREEN";
            Debug.Log("GREEN");
        }
        else
        {
            v = -1;
          //  result.text = "MISS";
            Debug.Log("MISS!");
        }

        if(sliderType == 0)
        {
            Debug.Log("@Sending values");
           if(PhotonNetwork.isMasterClient)
            {
                GameObject.FindGameObjectWithTag(PLAYER1_TAG).GetComponent<PlayerResults>()._atkValue = v;
            }
            else
            {
                GameObject.FindGameObjectWithTag(PLAYER2_TAG).GetComponent<PlayerResults>()._atkValue = v;
            }
        }else
        {
            if (PhotonNetwork.isMasterClient)
            {
                GameObject.FindGameObjectWithTag(PLAYER1_TAG).GetComponent<PlayerResults>()._blkValue= v;
            }
            else
            {
                GameObject.FindGameObjectWithTag(PLAYER2_TAG).GetComponent<PlayerResults>()._blkValue = v;
            }
        }
    }

    public void ToggleIsActivated()
    {
        isActivated = true;
        //Set random Succes value
        SetRandomSliderSucces();

    }

    private void SetRandomSliderSucces()
    {
        activeSliderIndex = Random.Range(1, 5);
        sliderBackground.sprite = sliderList[activeSliderIndex].sliderImage;
    }


   
}
