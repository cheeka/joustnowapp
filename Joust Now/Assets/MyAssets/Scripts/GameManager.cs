﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : Photon.PunBehaviour {

	enum Phases : byte
    {
        idle,
        starting,
        attacking,
        blocking,
        engaging
    };

    public enum player
    {
        player1,
        player2
    }

    public enum resolutions
    {
        atkSuccess,
        blocked,
        missed
    }

    public struct results
    {
        public player p;
        public int attackValue;
        public int blockValue;
        public bool isReady;
       
    }
    private const string PLAYER1_TAG = "Player1";
    private const string PLAYER2_TAG = "Player2";

    [SerializeField]
    private GameObject counterText;

    [SerializeField]
    private SliderController sliderA;
    [SerializeField]
    private SliderController sliderB;
    [SerializeField]
    private Transform enconunterPos1;
    [SerializeField]
    private Transform enconunterPos2;
   // private Vector2 encounterPos1V2;
 //   private Vector2 encounterPos2V2;
    [SerializeField]
    private Transform player1;
    [SerializeField]
    private Transform player2;
    [SerializeField]
    private float moveSpeed = 3f;
    [SerializeField]
    private CameraManager cameraManager;
    private float startCounter = 5f;
  //  private float finalCounter = 5f;
    [SerializeField]
    private Phases inPhase = Phases.idle;

    [SerializeField]
    private PlayerResults _playerResults1;
    [SerializeField]
    private PlayerResults _playerResults2;

    
    public Transform[] spawns;

   /* [SerializeField]
    private Transform spawn1;
    [SerializeField]
    private Transform spawn2;*/

    private byte winner;

    public byte _winner
    {
        get { return winner; }
        set { winner = value; }
    }



    void Start () {

        init();

    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(winner);
            
        }
        else
        {
            winner = (byte)stream.ReceiveNext();
           
        }
    }

    private void init()
    {
        if(PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.Instantiate("Player1", spawns[0].position, spawns[0].rotation,0);
        }else
        {
            PhotonNetwork.Instantiate("Player2", spawns[1].position, spawns[1].rotation, 0);
            
        }
        counterText.SetActive(false);
     //   encounterPos1V2 = new Vector2(enconunterPos1.position.x, enconunterPos1.position.y);
     //   encounterPos2V2 = new Vector2(enconunterPos2.position.x, enconunterPos2.position.y);
        StartCoroutine(AfterAnimationStart(7f));
    }

    private void Update()
    {
          if (Input.GetKeyDown(KeyCode.Escape))
        {
            PhotonNetwork.Disconnect();
            Application.Quit();
        }
                
        
        if (inPhase == Phases.starting && startCounter > 0)
        {
            PrepareLateParameters();
            startCounter -= Time.deltaTime;
            int count = (int)startCounter + 1;
            counterText.GetComponent<Text>().text = count+"";
        }else if (startCounter <= 0 && inPhase == Phases.starting)
        {
            counterText.GetComponent<Text>().text = "GO!";
            StartCoroutine(HideCounter(2f));
            inPhase = Phases.attacking;
            sliderA.ToggleIsActivated();
        }
        if (_playerResults1 == null || _playerResults2 == null)
            return;

        if (inPhase == Phases.blocking && _playerResults1._isReady && _playerResults2._isReady)
        {
          
            if (PhotonNetwork.isMasterClient)
            {
                Debug.Log("Calling engagin phase");
                CheckDefense(_playerResults1, _playerResults2);
                CheckDefense(_playerResults2, _playerResults1);
                //check winner
                CheckWinner();
                photonView.RPC("setEngagingPhase", PhotonTargets.All, (byte)Phases.engaging);
                if (winner != 3)
                {
                    photonView.RPC("RPCChangeScene", PhotonTargets.All, 5f,(byte)0);
                }
                else if(winner == 3)
                {
                  //  photonView.RPC("RPCChangeScene", PhotonTargets.All, 5f,(byte)1);
                }
               
              

            }
           
        }else if(Phases.engaging == inPhase)
        {
            MoveToCenter();
         
            cameraManager.movingToCenter = true;
        }

        
    }

    private void CheckWinner()
    {
        if(_playerResults1._pState == PlayerResults.state.died && _playerResults2._pState == PlayerResults.state.died)
        {
            _winner = 0; //Draw
        }else if(_playerResults1._pState != PlayerResults.state.died && _playerResults2._pState == PlayerResults.state.died)
        {
            _winner = 1; // Player 1 won
        }else if(_playerResults1._pState == PlayerResults.state.died && _playerResults2._pState != PlayerResults.state.died)
        {
            _winner = 2; //Player 2 won
        }else
        {

        _winner = 3; //Both missed 
        }
    }

    private void PrepareLateParameters()
    {
        player1 = GameObject.FindGameObjectWithTag(PLAYER1_TAG).transform;
        _playerResults1 = player1.GetComponent<PlayerResults>();
        player2 = GameObject.FindGameObjectWithTag(PLAYER2_TAG).transform;
        _playerResults2 = player2.GetComponent<PlayerResults>();
        if(player1 == null || _playerResults1 == null || player2 == null || _playerResults2 == null)
        {
            Debug.LogError("Can find player 1 components");
        }
    }
    
 
    [PunRPC]
    private void setEngagingPhase(byte phaseType)
    {
        inPhase = (Phases)phaseType;
        _playerResults1._animationState = 1;
        _playerResults2._animationState = 1;
    }

    private void CheckDefense(PlayerResults a,PlayerResults b)
    {
          if(b._atkValue == -1)
        {
            photonView.RPC("RPCChangePState", PhotonTargets.All, (byte)PlayerResults.state.avoided,a.tag);
           
        }else if(b._atkValue > a._blkValue)
        {
            photonView.RPC("RPCChangePState", PhotonTargets.All, (byte)PlayerResults.state.died, a.tag);
        }else if(b._atkValue <= a._blkValue)
        {
            photonView.RPC("RPCChangePState", PhotonTargets.All, (byte)PlayerResults.state.blocked, a.tag);
        }    
    }

    [PunRPC]
    public void RPCChangePState(byte st,string p)
    {
        Debug.Log("@At RPC change state");
        GameObject.FindGameObjectWithTag(p).GetComponent<PlayerResults>()._pState = (PlayerResults.state)st;
    }

    private void MoveToCenter()
    {

        player1.position = Vector2.MoveTowards(new Vector2(player1.position.x, player1.position.y), new Vector2(spawns[1].position.x,spawns[1].position.y),moveSpeed * Time.deltaTime);
        player2.position = Vector2.MoveTowards(new Vector2(player2.position.x, player2.position.y), new Vector2(spawns[0].position.x,spawns[0].position.y), moveSpeed * Time.deltaTime);

       /* if(new Vector2(player1.position.x, player1.position.y) == encounterPos1V2 || new Vector2(player2.position.x, player2.position.y) == encounterPos2V2)
        {
            //ejecutar animaciones
            if(_playerResults1._pState == PlayerResults.state.died)
            {
                player1.GetComponent<SpriteRenderer>().color = Color.red;
            }else if(_playerResults1._pState == PlayerResults.state.blocked)
            {
                player1.GetComponent<SpriteRenderer>().color = Color.blue;
            }
            else if (_playerResults1._pState == PlayerResults.state.avoided)
            {
                player1.GetComponent<SpriteRenderer>().color = Color.green;
            }

            if (_playerResults2._pState == PlayerResults.state.died)
            {
                player2.GetComponent<SpriteRenderer>().color = Color.red;
            }
            else if (_playerResults2._pState == PlayerResults.state.blocked)
            {
                player2.GetComponent<SpriteRenderer>().color = Color.blue;
            }
            else if (_playerResults2._pState == PlayerResults.state.avoided)
            {
                player2.GetComponent<SpriteRenderer>().color = Color.green;
            }

            inPhase = Phases.idle;
        }*/
        
    }

    private void ResetRound()
    {

    }

    public void NextPhase()
    {
        StartCoroutine(CallNextPhase(0.5f));
    }

    IEnumerator CallNextPhase(float time)
    {
        Debug.Log("@CallNextPhase");
        yield return new WaitForSeconds(time);
        if (inPhase == Phases.attacking)
        {
            inPhase = Phases.blocking;
            sliderB.ToggleIsActivated();
          
        }
        else if (inPhase == Phases.blocking)
        {
           
            if (PhotonNetwork.isMasterClient)
            {
                _playerResults1._isReady = true;
            }
            else
            {
                _playerResults2._isReady = true;
            }
        }
    }
    [PunRPC]
    private void RPCChangeScene(float t,byte sceneN)
    {
        StartCoroutine(ChangeScene(t,sceneN));
    }

    IEnumerator ChangeScene(float time,byte sceneN)
    {
        yield return new WaitForSeconds(time);
        if(sceneN == 0)
        {
            PhotonNetwork.LeaveRoom();
        }
        else
        {
            if(PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.LoadLevel("PlayScene");
            }
        }
        
     
    }

    IEnumerator HideCounter(float time)
    {
        Debug.Log("@HideCounter");
        yield return new WaitForSeconds(time);
        Debug.Log("Hiding counter");
        counterText.SetActive(false);
    }

    IEnumerator AfterAnimationStart(float time)
    {
        Debug.Log("@AfterAnimationStart");

        yield return new WaitForSeconds(time);
        Debug.Log("Starting counter");
        cameraManager.GetComponent<Animator>().enabled = false;
        inPhase = Phases.starting;
        counterText.SetActive(true);
        counterText.GetComponent<Text>().text = (int)startCounter + "";
    }
}
