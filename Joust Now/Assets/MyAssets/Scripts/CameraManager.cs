﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public int playerN = 1;
    Animator anim;
    [SerializeField]
    private float moveSpeed = 3f;
    [SerializeField]
    private Vector3 centerPos = new Vector3(0, -2,-10);
    public bool movingToCenter { get; set; }
    static string PLAYER1_TRIGGER = "StartPlayer1";
    static string PLAYER2_TRIGGER = "StartPlayer2";

    private void Awake()
    {
        anim = GetComponent<Animator>();
        if(PhotonNetwork.isMasterClient)
        {
            playerN = 1;
        }
        else
        {
            playerN = 2;
        }
    }


    private void Update()
    {
        if(movingToCenter)
        {
            MoveToCenter();
            Debug.Log("@MovingTocenter");
        }
    }


    private void MoveToCenter()
    {
        transform.position = Vector3.MoveTowards(transform.position, centerPos, moveSpeed * Time.deltaTime);
        if(transform.position ==  centerPos)
        {
            movingToCenter = false;
        }
    }

    void Start () {
		if(playerN == 1)
        {
            anim.SetTrigger(PLAYER1_TRIGGER);
        }else if(playerN == 2)
        {
            anim.SetTrigger(PLAYER2_TRIGGER);
        }
	}
	
	
}
