﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeManager : Photon.PunBehaviour {

    public GameObject playButton;
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public override void OnConnectedToMaster()
    {
        playButton.SetActive(true);
    }
}
